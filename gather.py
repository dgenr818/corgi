#!/usr/bin/env python3
# Copyright (c) 2020 The Bitcoin Cash Node developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import os

import utils


# get all the logs form the nodes
def gather(run_id=None):
    if run_id is None:
        run_id = utils.read_ceremony_id()        
    instances = utils.get_instances(run_id)

    def copy_file_to_local(instance, filename, localfilename=None):
        if localfilename is None:
            localfilename = filename

        region_name = instance["region"]
        ip = instance["ip"]
        keyfile = "{}/{}.pem".format(run_id, region_name)

        command = "scp -q -oStrictHostKeyChecking=no -i {} ubuntu@{}:~/{} {}/logs/{}-{}-{}".format(
            keyfile, 
            ip, 
            filename,
            run_id,
            instance["region"], 
            instance["id"],
            localfilename)
        os.system(command)

    def copy_files_to_local(instance) :

        copy_file_to_local(instance, "script.log")
        copy_file_to_local(instance, "node_actions.log")
        copy_file_to_local(instance, "hashtx.log")
        copy_file_to_local(instance, "hashblock.log")
        copy_file_to_local(instance, ".bitcoin/regtest/debug.log", "debug.log")

    tasks = []
    for instance in instances:
        tasks.append([copy_files_to_local, instance])
    utils.create_multiverse(tasks)    

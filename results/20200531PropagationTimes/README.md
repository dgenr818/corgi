This experiment was run in the evening of Sunday 31/05/2020. 

A total of 256 t3.micro instances in 20 locations with BCHN was run for a couple of hours, gathering data about incoming transactions. 

The data was gathered and cleaned. Since the nodes start logging transactions before the IBD is finished, only transactions after 19:30 CET were considered (ie, after the last node finished syncing) for the chart. Also some outliers were removed. 

The chart represents delay times in ascending order. Y-axis is time in seconds. X-axis is simply progressive sample number.

![Propagation times](propagation200531.jpg)